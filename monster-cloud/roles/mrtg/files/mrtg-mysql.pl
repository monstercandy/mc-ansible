#!/usr/bin/perl

my $mx = `mysqladmin status version`;
die "Cant find questions" if($mx !~ /Questions: (\d+)/);
print "$1\n";

die "Cant find slow queries" if($mx !~ /Slow queries: (\d+)/);
print "$1\n";

die "Cant find uptime" if($mx !~ /(?:.+?)Uptime:\s+(.+?)\n/s);
print "$1\n";

die "Cant find version" if($mx !~ /Server version\s+(.+)/);
print "$1\n";
