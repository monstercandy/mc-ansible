#!/usr/bin/perl
# 14:32:35 up  1:07,  1 user,  load average: 0.02, 0.06, 0.00
# 16:32:40 up  3:08,  2 users,  load average: 0.75, 0.43, 0.46
# 18:01:14 up  4:36,  1 user,  load average: 0.31, 0.33, 0.35
use Sys::Hostname;
my $h = hostname();
my $o = `uptime`;
my ($l1, $l2);
if ($o =~ /load average:\s+(\d+)\.(\d+),\s+\d+\.\d+,\s+\d+\.\d+/) {
  print "$1.$2\n$1.$2\n\n$h\n";
}else{
  print "0\n0\n\n$h\n";
}
