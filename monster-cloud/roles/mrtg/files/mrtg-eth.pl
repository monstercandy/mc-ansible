#!/usr/bin/perl

use Sys::Hostname;

my ($sumrx, $sumtx);
if(scalar @ARGV) {
  for my $d (@ARGV) {
    my ($arx, $atx) = getstat($d);
    $sumrx += $arx;
    $sumtx += $atx;
  }
} else {
  ($sumrx, $sumtx) = getstat("");
}
my $h = hostname();
print "$sumrx\n";
print "$sumtx\n";
print "\n";
print "$h $device\n";


sub getstat {
my $device = shift;
my $x = `/sbin/ifconfig $device`;
my $rx = 0;
if ($x =~ /RX packets\s+\d+\s+bytes\s+(\d+)/) {
  $rx = $1;
}
my $tx = 0;
if ($x =~ /TX packets\s+\d+\s+bytes\s+(\d+)/) {
  $tx = $1;
}

return ($rx, $tx);
}
