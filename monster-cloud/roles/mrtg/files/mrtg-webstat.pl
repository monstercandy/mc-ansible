#!/usr/bin/perl

use strict;
use warnings;
use LWP::Simple;
use Sys::Hostname;

my $password = shift @ARGV;
die "Usage: $0 password" if(!$password);

my $h = hostname;

my $nginx_url = "http://$h/nginx-status/$password";
my $n = get($nginx_url) || "";

my $n_ctr = 0;
$n_ctr = $1 if($n =~ /server accepts handled requests\s+(\d+)/);

my $apache_url = "http://127.0.0.1:82/apache-status/";
my $a = get($apache_url) || "";
my $a_ctr = 0;
$a_ctr = $1 if($a =~ /<dt>Total accesses: (\d+) - Total/);

print "$n_ctr\n$a_ctr\n\n$h\n";
