<?php
$titleArr = Array(
  "hu" => "Weboldal nincs feltöltve",
  "en" => "Website is not yet uploaded",
);
$messageArr = Array(
  "hu" => "A weboldal tulajdonosa még nem töltött fel tartalmat.",
  "en" => "The owner of this website has not uploaded any content yet.",
);

$lang = strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
if(!$titleArr[$lang]) $lang = "en";

$title = $titleArr[$lang];
$message = $messageArr[$lang];
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Title" content="<?=$_SERVER["HTTP_HOST"]?> - <?=$title?>">
<title><?=$_SERVER["HTTP_HOST"]?> - <?=$title?></title>
<style type="text/css">
<!--
body {
 margin-left: 0px;
 margin-top: 0px;
 margin-right: 0px;
 margin-bottom: 0px;
}
#content {
  background: url(background.png) no-repeat; 
  width: 1000px; 
  height: 379px;
  padding-left: 40px;  
  padding-top: 30px;  
  font-family: Sans-serif;
  font-size: 20px;
  text-shadow: 4px 4px 8px white;
}
a {
  color: black;
  text-decoration: none;
}
h2 {
  max-width: 600px;
}
-->
</style>
</head>
<body>
<a href="https://www.{{ service_main_domain }}">
 <div id="content">
   <h2><?=$message?></h2>
 </div>
</a>
</body>
</html>