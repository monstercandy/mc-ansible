---
- name: installing hacktest files
  vars:
    database_name: "mc_hacktest"
    database_pass: "{{ lookup('password', inventory_group+'/'+server_name+'/storages/'+ storage.wh_name +'_database_'+ database_name +'.password') }}"
    phps: "{{ lookup('template', 'phps.j2') | from_yaml }}"
    hacktest_hostname: "hacktest.{{ server_full_hostname }}"
    mail_domain: "{{ hacktest_hostname }}"
    mail_user1: "mail1@{{ mail_domain }}"
    mail_user1_alias: "mail1-alias@{{ mail_domain }}"
    mail_user2: "mail2@{{ mail_domain }}"
    mail_pass: "{{ lookup('password', inventory_group + '/' + server_name + '/hacktest-mail-password.pw chars=ascii_letters length=15') }}"
  block:
    - name: displaying all the hacktest urls
      debug:
        msg: "http://php-{{ item|replace('.','') }}.{{ server_name }}.{{ service_main_domain }}/test.php?access_password={{ lookup('password', inventory_group+'/'+server_name+'/hacktest.pw chars=ascii_letters length=15') }}"
      with_items: "{{ php_server_versions }}"

    - name: creating a webstore first
      vars:
        storage:
          name: "hacktest"
          hostname: "{{ hacktest_hostname }}"
          ignoreMailPolicyErrors: True
          template_override:
            t_max_container_apps: 30
      include_role:
        name: api_create_webstorage

    - name: mail tests
      vars:
        it_storage: "{{ lookup('file', inventory_group+'/'+server_name+'/storages/hacktest.yaml') | from_yaml }}"
        userid: "{{ it_storage.wh_user_id }}"
      block:
        - name: attaching domain to the email subsystem
          vars:
            payload:
              do_domain_name: "{{ mail_domain }}"
              do_webhosting: "{{ it_storage.wh_id }}"
              account: "{{ userid }}"
          shell: |
            /opt/client-libs-for-other-languages/internal/perl/generic-light.pl email PUT /email/domains '{{ payload | to_json }}'

        - name: creating email accounts
          vars:
            payload:
              account: "{{ userid }}"
              ea_email: "{{ item }}"
              ea_password: "{{ mail_pass }}"
              ea_quota_bytes: "{{ 50*1024*1024 }}"
              ea_autoexpunge_days: 0
          shell: |
            /opt/client-libs-for-other-languages/internal/perl/generic-light.pl email PUT \
               /email/webhostings/{{ it_storage.wh_id }}/domains/domain/{{ mail_domain }}/accounts '{{ payload | to_json }}'
          register: account_created
          failed_when: ((account_created.rc != 0) and (not('"ALREADY_EXISTS"' in account_created.stderr)))
          with_items:
            - "{{ mail_user1 }}"
            - "{{ mail_user2 }}"

        - name: "creating email alias 1 {{ mail_user1_alias }} -> {{ mail_user1 }}"
          vars:
            payload:
              account: "{{ it_storage.wh_user_id }}"
              el_alias: "{{ mail_user1_alias }}"
              el_destination: "{{ mail_user1 }}"
              el_verified: Yes
          shell: |
            /opt/client-libs-for-other-languages/internal/perl/generic-light.pl email PUT \
               /email/webhostings/{{ it_storage.wh_id }}/domains/domain/{{ mail_domain }}/aliases '{{ payload | to_json }}'
          register: alias_created
          failed_when: ((alias_created.rc != 0) and (not('"ALREADY_EXISTS"' in alias_created.stderr)))

        - name: "creating email alias 2 {{ mail_user2 }} -> {{ mail_user1 }}"
          vars:
            payload:
              account: "{{ it_storage.wh_user_id }}"
              el_alias: "{{ mail_user2 }}"
              el_destination: "{{ mail_user1 }}"
              el_verified: Yes
          shell: |
            /opt/client-libs-for-other-languages/internal/perl/generic-light.pl email PUT \
               /email/webhostings/{{ it_storage.wh_id }}/domains/domain/{{ mail_domain }}/aliases '{{ payload | to_json }}'
          register: alias_created
          failed_when: ((alias_created.rc != 0) and (not('"ALREADY_EXISTS"' in alias_created.stderr)))

        - name: "creating email alias 2 {{ mail_user2 }} -> {{ mail_user2 }}"
          vars:
            payload:
              account: "{{ it_storage.wh_user_id }}"
              el_alias: "{{ mail_user2 }}"
              el_destination: "{{ mail_user2 }}"
              el_verified: Yes
          shell: |
            /opt/client-libs-for-other-languages/internal/perl/generic-light.pl email PUT \
               /email/webhostings/{{ it_storage.wh_id }}/domains/domain/{{ mail_domain }}/aliases '{{ payload | to_json }}'
          register: alias_created
          failed_when: ((alias_created.rc != 0) and (not('"ALREADY_EXISTS"' in alias_created.stderr)))


    - name: creating a database
      vars:
        storage: "{{ lookup('file', inventory_group+'/'+server_name+'/storages/hacktest.yaml') | from_yaml }}"
        database:
          name: "{{ database_name }}"
      include_role:
        name: api_create_database

    - name: web applications
      vars:
        storage: "{{ lookup('file', inventory_group+'/'+server_name+'/storages/hacktest.yaml') | from_yaml }}"
        webapp_root: "{{ storage.extras.web_path}}/docker-apps"
      block:

        - name: the default webapp shall be none
          shell: |
            /opt/client-libs-for-other-languages/internal/perl/generic-light.pl docroot POST '/docrootapi/docroots/webapps/{{ storage.wh_id }}' \
              '{"type": "none" }'

        - name: cloning the example webapps root
          become: true
          become_user: "{{ storage.wh_id }}"
          block:
            - name: creating the main dir
              file:
                path: "{{ webapp_root }}"
                state: directory

            - name: cloning the repo
              git:
                repo: 'https://monsterbb@bitbucket.org/monsterbb/mc-webapp-examples.git'
                dest: "{{ webapp_root }}"
              register: webapp_git_result

            - template:
                src: hacktest-config.ini
                dest: "{{ webapp_root }}/hacktest-config.ini"

        - name: installing webapps
          include_role:
            name: hacktest_webapp
          with_items:
            - category: "nodejs-express"
              image: "node-12.7-alpine-nodejs"
              container_name: "hacktest_nodejs_express"
              wh_id: "{{storage.wh_id}}"
              restart: "{{webapp_git_result.changed}}"
              expected_hello: "Hello World from a Node.JS+Express application!"
            - category: "python-django"
              image: "alpine-3.10-python"
              container_name: "hacktest_python_django"
              wh_id: "{{storage.wh_id}}"
              subdir: "mysite"
              wsgi: "mysite.wsgi"
              restart: "{{webapp_git_result.changed}}"
              expected_hello: "Hello from a Django view!"
            - category: "python-flask"
              image: "alpine-3.10-python"
              container_name: "hacktest_python_flask"
              wh_id: "{{storage.wh_id}}"
              wsgi: "wsgi"
              restart: "{{webapp_git_result.changed}}"
              expected_hello: "Hello from a Python Flask application"
          loop_control:
            loop_var: webapp

        - name: installing PHP based webapps
          vars:
            phps: "{{ lookup('template', 'phps.j2') | from_yaml }}"
          block:
            - name: phps on this site
              debug:
                var:
                  phps

            - name: real work
              include_role:
                name: hacktest_webapp
              with_items: "{{ phps }}"
              loop_control:
                loop_var: webapp

    - name: invoking the tests of PHP hacktest
      vars:
        storage: "{{ lookup('file', inventory_group+'/'+server_name+'/storages/hacktest.yaml') | from_yaml }}"
      include_role:
        name: hacktest_php
      with_items: "{{ phps }}"
      loop_control:
        loop_var: webapp
