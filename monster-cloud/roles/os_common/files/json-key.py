#!/usr/bin/python

import sys
import json

if len(sys.argv) != 3:
    print "Usage: json.py file key"
    sys.exit(1)

file = sys.argv[1]
key = sys.argv[2]

data = {}
try:
    with open(file) as data_file:
        data = json.loads(data_file.read())
        print data[key]
except IOError:
    pass
