#!/usr/bin/perl

use File::Slurp;

my $pid = $ARGV[0];
die "Usage: $0 pid" if((!$pid)||(!-d "/proc/$pid"));

my $t = read_file("/proc/$pid/stat");
my @s = split(/ /, $t);
my @x = (
 "pid",
 "comm",
 "state",
 "ppid",
 "pgrp",
 "session",
 "tty_nr",
 "tpgid",
 "flags",
 "minflt",
 "cminflt",
 "majflt",
 "cmajflt",
 "utime",
 "stime",
 "cutime",
 "cstime",
 "priority",
 "nice",
 "num_threads",
 "itrealvalue",
 "starttime",
 "vsize",
 "rss",
 "rsslim",
 "startcode",
 "endcode",
 "startstack",
 "kstkesp",
 "kstkeip",
 "signal",
 "blocked",
 "sigignore",
 "sigcatch",
 "wchan",
 "nswap",
 "cnswap",
 "exit_signal",
 "processor",
 "rt_priority",
 "policy",
 "delayacct_blkio_ticks",
 "guest_time",
 "cguest_time",
);
for (my $i=0; $i<scalar @x; $i++) {

  print $x[$i].": ".$s[$i]."\n";
}
