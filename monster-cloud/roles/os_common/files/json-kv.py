#!/usr/bin/python

import sys
import json

if len(sys.argv) != 4:
    print "Usage: json.py file key value"
    sys.exit(1)

file = sys.argv[1]
key = sys.argv[2]
value = json.loads(sys.argv[3])

data = {}
try:
    with open(file) as data_file:
        data = json.loads(data_file.read())
except IOError:
    pass

data[sys.argv[2]] = value
print json.dumps(data)
