#!/bin/bash

BOUNCE={{cert_api_var_root}}/.bounce
if [ ! -e "$BOUNCE" ]; then
  exit 0
fi

rm "$BOUNCE"

echo reloading services that use certificate aliases

drehup buster-nginx

{% if (inventory_hostname in groups.mail_servers) %}
  drehup buster-dovecot-2.3
  drehup buster-postfix
{% endif %}

