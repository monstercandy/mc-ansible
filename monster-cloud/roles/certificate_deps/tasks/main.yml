---
- name: ensuring certificate dependencies
  vars:
    cert_api_var_root: "{{ api_lib_dir_root_path }}/cert"
    letsencrypt_root: "{{ cert_api_var_root }}/letsencrypt"
    letsencrypt_logs_root: /var/log/monster/cert/letsencrypt
    cert_api_var_active_root: "{{ cert_api_var_root }}/store/active"
    local_easy_rsa_root: /root/.ansible/roles/clvx.easy-rsa/files
  block:
    - name: some directories must be present
      file:
        path: "{{ item }}"
        state: directory
        owner: "{{ api_default_user }}"
        group: "{{ api_default_group }}"
      with_items:
        - "{{ cert_api_var_active_root }}"
        - "{{ mc_cert_root }}"
        - "{{ letsencrypt_root }}"
        - "{{ letsencrypt_logs_root }}"

    - name: Registering for Let's Encrypt
      shell: |
        if test -n "$(find /var/lib/monster/cert/letsencrypt/etc/accounts/acme-v*.api.letsencrypt.org/directory -wholename '*/private_key.json' -print -quit)"; then
           echo "Let's Encrypt registration is already present"
           exit 0
        fi

        echo Registering for Lets Encrypt
        chroot --userspec={{ api_default_user }}:{{ api_default_group }} / certbot register --config-dir {{ letsencrypt_root }}/etc --work-dir {{ letsencrypt_root }}/var --logs-dir {{ letsencrypt_logs_root }} --agree-tos -m {{ tech_admin_email }}
      register: letsencrypt_registration_result
      changed_when: not("already present" in letsencrypt_registration_result.stdout)
      when: letsencrypt_registration | default(True)

    - name: populating components of the trust network
      block:
        - name: the CA certificate
          copy:
            src: "{{ local_easy_rsa_root }}/ca.crt"
            dest: "{{ mc_cert_root }}/ca.crt"

        - name: the client and server certificate files
          copy: src={{ item.src }} dest={{ item.dest }} mode={{ item.mode }}
          with_items:
            - { src: "{{ local_easy_rsa_root }}/client_{{ server_full_hostname }}.crt", dest: "{{ mc_cert_root }}/client.crt", mode: "u+rw,g+r,o+r" }
            - { src: "{{ local_easy_rsa_root }}/client_{{ server_full_hostname }}.key", dest: "{{ mc_cert_root }}/client.key", mode: "u+rw,g-rwx,o-rwx" }
            - { src: "{{ local_easy_rsa_root }}/{{ server_full_hostname }}.crt", dest: "{{ mc_cert_root }}/server.crt", mode: "u+rw,g+r,o+r" }
            - { src: "{{ local_easy_rsa_root }}/{{ server_full_hostname }}.key", dest: "{{ mc_cert_root }}/server.key", mode: "u+rw,g-rwx,o-rwx" }

        - name: creating key+crt=pem version of them
          shell: |
            set -e
            cd {{ mc_cert_root }}
            if [ ! -s server.pem ]; then
              echo creating server.pem
              cat server.crt server.key > server.pem
              chmod 600 server.pem
            fi

            if [ ! -s client.pem ]; then
              echo creating client.pem
              cat client.crt client.key > client.pem
              chmod 600 client.pem
            fi
          register: pem_result
          changed_when: ("creating" in pem_result.stdout)

    - name: Pointing to the trustnetworks server certificate when there is no dedicated there yet
      environment:
        CERTIFICATE_DIRECTORY: "{{ cert_api_var_active_root }}"
      shell: |
        set -e
        cd $CERTIFICATE_DIRECTORY
        if [ -s "main.fullchain.crt" ] && [ -s "main.key" ]; then
           echo "Certificate already exists"
           exit 0
        fi

        for i in main.key main.crt main.fullchain.crt main.pem main.intermediate.crt; do
           if [ -e "$i" ]; then
              echo "$i exists, removing"
              rm "$i"
           fi
           if [ -L "$i" ]; then
              echo "$i is a symlink, removing"
              rm "$i"
           fi
        done

        ln -s {{ mc_cert_root }}/server.key main.key
        ln -s {{ mc_cert_root }}/server.crt main.crt
        ln -s {{ mc_cert_root }}/server.pem main.pem
        ln -s main.crt main.fullchain.crt
        # postfix complains if the intermediate file is empty, so we rather specify the ca here
        ln -s {{ mc_cert_root}}/ca.crt main.intermediate.crt
      register: selfsigned_result
      changed_when: not("already exists" in selfsigned_result.stdout)

    - name: ensuring our root CA is trusted
      block:
        - name: Ensure local certs directory exists
          file:
            state: directory
            path: /usr/local/share/ca-certificates

        - name: Symlinking root CA
          file:
            state: link
            src: "{{ mc_cert_root }}/ca.crt"
            dest: /usr/local/share/ca-certificates/mc.crt
          register: ca_link

        - name: Update cert index
          command: /usr/sbin/update-ca-certificates
          when: ca_link.changed

    - name: Generate Diffie Hellman parameters
      block:
        - name: Generate Diffie Hellman parameters
          shell: |
            set -e
            cd {{ mc_cert_root }}
            if [ -s "dhparams.pem" ]; then
              echo "dhparams.pem already exists"
              exit 0
            fi
            openssl dhparam -out dhparams.pem 2048
          register: dhparams_result
          changed_when: not("already exists" in dhparams_result.stdout)
          when: perhost_diffie_hellman_keys|default(True) == True

        - name: Copy Diffie Hellman parameters
          copy:
            src: dhparams.pem
            dest: "{{ mc_cert_root }}/dhparams.pem"
            force: no
          register: dhparams_result
          when: perhost_diffie_hellman_keys|default(True) == False

    - name: Generate SSH keys
      shell: |
        if [ ! -e '{{ mc_cert_ssh_rsa_key }}' ]; then
           ssh-keygen -m PEM -t rsa -N "" -f '{{ mc_cert_ssh_rsa_key }}'
        fi
        if [ ! -e '{{ mc_cert_ssh_dsa_key }}' ]; then
           ssh-keygen -m PEM -t dsa -N "" -f '{{ mc_cert_ssh_dsa_key }}'
        fi
        if [ ! -e '{{ mc_cert_ssh_ecdsa_key }}' ]; then
           ssh-keygen -m PEM -t ecdsa -N "" -f '{{ mc_cert_ssh_ecdsa_key }}'
        fi

    - name: cert-rehup.sh for bouncing configs
      vars:
        dest_path: /usr/local/sbin/cert-rehup.sh
      block:
        - name: uploading the cert rehup script
          template:
            src: cert-rehup.sh
            dest: "{{ dest_path }}"
            mode: 0755

        - name: configuring crontab entry for cert bounce
          cron:
            name: checking certificate bounce request every minute
            minute: "*/1"
            job: "{{ dest_path }}"

    - name: checking whether the email system has a dedicated certificate
      shell: |
        ls {{ cert_api_var_active_root }}/mail.crt
      ignore_errors: yes
      register: mail_cert

    - name: registering mail certificate pathes
      set_fact:
        mail_cert_cert_path: "{{ cert_api_var_active_root }}/mail.crt"
        mail_cert_key_path: "{{ cert_api_var_active_root }}/mail.key"
        mail_cert_intermediate_path: "{{ cert_api_var_active_root }}/mail.intermediate.crt"
        mail_cert_fullchain_path: "{{ cert_api_var_active_root }}/mail.fullchain.crt"
        mail_cert_pem_path: "{{ cert_api_var_active_root }}/mail.pem"
      when: mail_cert.rc == 0
