server {

  include common/listen.conf;
  server_name {{ server_full_hostname }} {{ server_name }};
  set $mm_webhosting_id "api-frontend";

  access_log "/var/log/nginx/api-frontend.log" main;
  set $doc_root "/var/lib/monster/frontend/html.release";

  include common/vhost.conf;
  root $doc_root;

  if ($scheme = http) {
    return 301 https://$server_name$request_uri;
  }

  # this one is for mrtg requests
  location ^~ /nginx-status/{{ lookup('password', inventory_group+'/'+server_name+'/nginx_status_pwd chars=ascii_letters length=32') }} {

    access_log   off;

    stub_status on;
  }

  location ^~ /mrtg {

    auth_basic           "closed site";
    auth_basic_user_file {{ nginx_lib_dir_path }}/auth.d/_admin.txt;
    alias {{ mrtg_lib_dir_path }};

  }

  location ^~ /nginx-status {

    auth_basic           "closed site";
    auth_basic_user_file {{ nginx_lib_dir_path }}/auth.d/_admin.txt;

    stub_status on;
  }

  location ^~ /apache-status {

    auth_basic           "closed site";
    auth_basic_user_file {{ nginx_lib_dir_path }}/auth.d/_admin.txt;

    try_files non_existent @proxy;
  }

  location ^~ /nginx-vts {

    auth_basic           "closed site";
    auth_basic_user_file {{ nginx_lib_dir_path }}/auth.d/_admin.txt;

    vhost_traffic_status_display;
    vhost_traffic_status_display_format html;
  }

{% if (inventory_hostname in groups.installatron_servers) %}
  location ^~ /installatron {

    set $vhost 127.0.0.3;
    include common/params_proxy;
    proxy_pass http://127.0.0.3:1080;
  }
{% endif %}

  location ^~ /api {

    set $vhost 127.0.0.1;
    include common/params_proxy;
    proxy_pass http://127.0.0.1:{{ lookup('file', inventory_group + '/'+server_name+'/services/relayer_port') }};
  }

}
