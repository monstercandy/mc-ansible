window.mcConfiguration = {
       "default_country_code": "{{ frontend.country_code }}",
       "api_uri_prefix": "/api",
       "alert_timeout_ms": 10000,
       "template_version":"{{api_MonsterFrontend.after}}",
       "registration_extra_email_warning": false,
       "registrationTelOptional": {{ frontend.registration_tel_optional }},

       "pureNg": true,

       "serverUniqueDkim": true,

       "docker_webapp_examples_url": "https://www.bitbucket.org/monsterbb/mc-webapp-examples",

       "certPurchaseServer": "{{ hostvars[groups.accountapi_servers[0]].server_name }}",

       "customer_service_url": "{{ customer_service_url }}",
       "service_primary_domain": "{{ service_main_domain }}",

       "itemsPerPage": 20,

       "send_ping_after_inactivity": 10000,

       "template_directory": "/template.mc",

       "domain_dns_records_reset_template": "{{ service_main_domain }}",
       "nameservers": "a.ns.{{ service_main_domain }}, b.ns.{{ service_main_domain }}",

       "sms_notification_support": true,

       "dummy_tailer":"value"
}
