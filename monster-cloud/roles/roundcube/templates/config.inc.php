<?php

/* Local configuration for Roundcube Webmail */

// PEAR database DSN for read/write operations
// format is db_provider://user:password@host/database
// currently supported db_providers: mysql, mysqli, pgsql, sqlite, mssql
$config['db_dsnw'] = 'mysqli://{{ database_user }}:{{ database_pass }}@{{ server_full_hostname }}/{{ database_name }}';

// the mail host chosen to perform the log-in
// leave blank to show a textbox at login, give a list of hosts
// to display a pulldown menu or set one host as string.
// To use SSL/TLS connection, enter hostname with prefix ssl:// or tls://
{% if(roundcube_global is defined)%}

  $config['default_host'] =  Array(
{% for host in groups.mail_servers %}
    'tls://{{ hostvars[host].server_name }}.{{ service_main_domain }}' => "{{ hostvars[host].server_name }}: https://webmail.{{ hostvars[host].server_name }}.{{ service_main_domain }}",
{% endfor %}
  );

{% else %}
$config['default_host'] = '{{ server_full_hostname }}';
{% endif %}

// use this host for sending mails.
// to use SSL connection, set ssl://smtp.host.com
// if left blank, the PHP mail() function is used
$config['smtp_server'] = '{{ "tls://" if(roundcube_global is defined) else "" }}%h';

// SMTP username (if required) if you use %u as the username RoundCube
// will use the current username for login
$config['smtp_user'] = '%u';

// SMTP password (if required) if you use %p as the password RoundCube
// will use the current user's password for login
$config['smtp_pass'] = '%p';

// provide an URL where a user can get support for this Roundcube installation
// PLEASE DO NOT LINK TO THE ROUNDCUBE.NET WEBSITE HERE!
$config['support_url'] = '{{ customer_service_url }}';

// use this folder to store log files (must be writeable for apache user)
// This is used by the 'file' log driver.
$config['log_dir'] = '../logs/';

// use this folder to store temp files (must be writeable for apache user)
$config['temp_dir'] = 'temp/';

// Allow browser-autocompletion on login form.
// 0 - disabled, 1 - username and host only, 2 - username, host, password
$config['login_autocomplete'] = 2;

// this key is used to encrypt the users imap password which is stored
// in the session record (and the client cookie if remember password is enabled).
// please provide a string of exactly 24 chars.
$config['des_key'] = '{{ lookup("password", inventory_group+"/"+server_name+"/roundcube_des_key length=24") }}';

// use this name to compose page titles
$config['product_name'] = 'roundCube - {{ server_name }} - {{ service_name_human_long }}';

// end of config file
$config['plugins'] = array(

  // official plugins:
  'archive','markasjunk','emoticons','vcard_attachments','zipdownload','managesieve',

  // third party plugins:
  'attachment_preview',

  // mc plugins:
  'email_public',

);


$config['managesieve_vacation'] = 1;
$config['managesieve_host'] = '{{ "tls://" if(roundcube_global is defined) else "" }}%h';


#'idle_timeout'

// default sort col
$config['message_sort_col'] = 'date';

// These cols are shown in the message list. Available cols are:
// subject, from, to, cc, replyto, date, size, flag, attachment
$config['list_cols'] = array('subject', 'from', 'date', 'size', 'flag', 'attachment');

// the default locale setting (leave empty for auto-detection)
// RFC1766 formatted language name like en_US, de_DE, de_CH, fr_FR, pt_BR
// $config['language'] = 'hu_HU';

// use this format for detailed date/time formatting
$config['date_long'] = 'd.m.Y H:i';

// store draft message is this mailbox
// leave blank if draft messages should not be stored
$config['drafts_mbox'] = 'INBOX.Drafts';

// store spam messages in this mailbox
$config['junk_mbox'] = 'INBOX.Junk';

// store sent message is this mailbox
// leave blank if sent messages should not be stored
$config['sent_mbox'] = 'INBOX.Sent';

// move messages to this folder when deleting them
// leave blank if they should be deleted directly
$config['trash_mbox'] = 'INBOX.Trash';

// display these folders separately in the mailbox list.
// these folders will also be displayed with localized names
// NOTE: Use folder names with namespace prefix (INBOX. on Courier-IMAP)
$config['default_folders'] = array('INBOX', 'INBOX.Drafts', 'INBOX.Sent', 'INBOX.Junk', 'INBOX.Trash');

//$config['imap_debug'] = true;
//$config['debug_level'] = 1;

// automatically create the above listed default folders on login
$config['create_default_folders'] = true;

/***** these settings can be overwritten by user's preferences *****/// skin name: folder from skins/
$config['skin'] = 'larry';

$config['mime_types'] = '/etc/mime.types';

// Encoding of long/non-ascii attachment names:
// 0 - Full RFC 2231 compatible
// 1 - RFC 2047 for 'name' and RFC 2231 for 'filename' parameter (Thunderbird's default)
// 2 - Full 2047 compatible
$config['mime_param_folding'] = 0;

// lifetime of message cache
// possible units: s, m, h, d, w
$config['message_cache_lifetime'] = '10d';

// use this format for today's date display
$config['date_today'] = 'H:i';

// is daylight saving On?
$config['dst_active'] = true;

// focus new window if new message arrives
$config['focus_on_new_message'] = true;

//-------------------------------------------------- email public plugin:
$config['email_public_url'] = "https://{{ service_main_domain }}/min.php?op=ng&state=mc-email-public&server=[server_name]&email=";

