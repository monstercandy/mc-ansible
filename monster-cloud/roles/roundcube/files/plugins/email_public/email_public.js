/* Show user-info plugin script */

if (window.rcmail) {
  rcmail.addEventListener('init', function(evt) {
    // <span id="settingstabdefault" class="tablink"><roundcube:button command="preferences" type="link" label="preferences" title="editpreferences" /></span>
    var tab = $('<span>').attr('id', 'settingstabpluginemailpublic').addClass('tablink');
    
    var button = $('<a>').attr('href', rcmail.env.comm_path+'&_action=plugin.email_public').html(rcmail.gettext('email_public', 'email_public')).appendTo(tab);
    button.bind('click', function(e){ return rcmail.command('plugin.email_public', this) });
    
    // add button and register command
    rcmail.add_element(tab, 'tabs');
    rcmail.register_command('plugin.email_public', function(){ rcmail.goto_url('plugin.email_public') }, true);
  })
}

