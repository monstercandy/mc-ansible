<?php

class email_public extends rcube_plugin {

    public function init() {
        $this->add_texts('localization/', array('email_public'));
        $this->include_script('email_public.js');
        $this->register_action('plugin.email_public', array($this, 'email_public_init'));
    }

    public function email_public_init() {
        $this->register_handler('plugin.body', array($this, 'infohtml'));
        rcmail::get_instance()->output->send('plugin');
    }

    public function infohtml(){


      $rcmail = rcmail::get_instance();
      $user = $rcmail->user;

      $email = $user->data['username'];
      $email_public_url = $rcmail->config->get('email_public_url'); // $delimiter."Junk";//$folder_in.

      $host = $user->data['mail_host'];
      $hosta = explode(".", $host);
      $server_name = $hosta[0];
      $email_public_url = str_replace("[server_name]", $server_name, $email_public_url);

      $form = "<iframe style='border:0px; width: 100%; height: 100%' src='$email_public_url".urlencode($email)."'></iframe>";

      return $form;

   }
}

?>