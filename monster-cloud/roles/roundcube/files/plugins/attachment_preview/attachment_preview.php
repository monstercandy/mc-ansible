<?php
/**
 * Attachment Preview
 *
 * A plugin to preview upload attachments
 *
 * @version 1.0
 * @author Thomas Yu - Sian , Liu
 * @url https://github.com/thomasysliu/Roundcube-Plugin-Attachment-Preview
 * 
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
class attachment_preview extends rcube_plugin
{
    public $task = 'mail';

    function init()
    {
	$rcmail = rcmail::get_instance();
	if($rcmail->action == 'compose') {
	    $this->include_script('attachment_preview.js');
	    $this->include_stylesheet('attachment_preview.css');
	    $this->add_texts('localization/', true);
	}
	$this->register_action('plugin.preview', array($this, 'preview'));
	$this->register_action('plugin.download', array($this, 'download'));

    }
    function download()
    {

	$RCMAIL = rcmail::get_instance();
	$COMPOSE_ID = get_input_value('_id', RCUBE_INPUT_GPC);
	if( isset( $_SESSION['compose_data'] ) ){
	    $_SESSION['compose'] = $_SESSION['compose_data'][$COMPOSE_ID]; // After roundcube version 4542 
	}
	else{
	    $_SESSION['compose']['id'] = $COMPOSE_ID;  // Before roundcube version 4542
	}
	if (preg_match('/^rcmfile(\w+)$/', $_GET['_file'], $regs))
	    $id = $regs[1];


	if ($attachment = $_SESSION['compose_data_'.$COMPOSE_ID]['attachments'][$id])
	    $attachment = $RCMAIL->plugins->exec_hook('attachment_display', $attachment);

	if ($attachment['status']) {
	    if (empty($attachment['size']))
		$attachment['size'] = $attachment['data'] ? strlen($attachment['data']) : @filesize($attachment['path']);


	    if (file_exists($attachment['path'])) {
		$ua = $_SERVER["HTTP_USER_AGENT"]; 
		$filename = $attachment['name'];
		$encoded_filename = urlencode($filename); 
		$encoded_filename = str_replace("+", "%20", $encoded_filename); 
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		if (preg_match("/MSIE/", $ua)) { 
		    header('Content-Disposition: attachment; filename="' . $encoded_filename . '"'); 
		} else if (preg_match("/Firefox/", $ua)) { 
		    header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"'); 
		} else { 
		    header('Content-Disposition: attachment; filename="' . $filename . '"'); 
		}
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize( $attachment['path'] ));
		ob_clean();
		flush();
		readfile($attachment['path']);

	    }

	}
	exit;
    }
}

?>
