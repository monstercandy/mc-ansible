<?php
/* Servers configuration */
$i = 0;

$i++;
$cfg['Servers'][$i]['verbose'] = 'phpMyAdmin - {{ server_name }} - {{ service_name_human_long }}';
$cfg['Servers'][$i]['host'] = '{{ server_full_hostname }}';
$cfg['Servers'][$i]['port'] = 3306;
$cfg['Servers'][$i]['connect_type'] = 'tcp';
$cfg['Servers'][$i]['auth_type'] = 'cookie';
$cfg['Servers'][$i]['AllowRoot'] = false;

/* End of servers configuration */

$cfg['blowfish_secret'] = '{{ lookup("password", inventory_group+"/"+server_name+"/phpmyadmin_blowfish length=100") }}';
$cfg['UploadDir'] = '';
$cfg['SaveDir'] = '';
$cfg['BZipDump'] = false;
$cfg['DefaultLang'] = 'en';
$cfg['ServerDefault'] = 1;
$cfg['TempDir'] = '/tmp';
?>